from django.apps import AppConfig


class QuestionAnswerAppConfig(AppConfig):
    name = 'question_answer_app'

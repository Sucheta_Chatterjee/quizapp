from django.shortcuts import render
from .models import *
from rest_framework import mixins, viewsets
from .serializers import *

# Create your views here.
class QuestionCreate(
        mixins.CreateModelMixin,
        viewsets.GenericViewSet
    ):
    """
    @name : QuestionCreate,
    @description: this will create questions
    """
    queryset = Questions.objects.all()
    serializer_class = QuestionSerializer
    
    
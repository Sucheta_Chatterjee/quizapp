from django.db import models
from custom_user_app.models import CustomUser

# Create your models here.
class Questions(models.Model):
    """
    This model stores all question made by admin.
    """
    user = models.ForeignKey('custom_user_app.CustomUser', on_delete=models.CASCADE)
    set_no = models.IntegerField(null=True, blank=True)
    question = models.TextField(null=True, blank=True)
    options_a = models.TextField(null=True, blank=True)
    options_b = models.TextField(null=True, blank=True)
    options_c = models.TextField(null=True, blank=True)
    options_d = models.TextField(null=True, blank=True)
    correct_opt = models.CharField(max_length=5, null=True, blank=True)
    
    class Meta:
        verbose_name = 'Questions'
        db_table = 'questions'


class QuizResult(models.Model):
    """
    This model stores all question made by admin.
    """
    user = models.ForeignKey('custom_user_app.CustomUser', on_delete=models.CASCADE)
    questions = models.ForeignKey('Questions', on_delete=models.CASCADE)
    answer = models.CharField(max_length=5, null=True, blank=True)
    is_correct = models.BooleanField(null=True, blank=True)
    
    class Meta:
        verbose_name = 'Quiz Result'
        db_table = 'quiz_result'
    
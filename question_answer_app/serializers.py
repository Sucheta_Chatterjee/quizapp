from rest_framework import serializers
from .models import *

class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Questions
        fields = '__all__'
        
    def create(self, validated_data):
        request = self.context['request']
        user = request.user
        
        return super(QuestionSerializer, self).create(validated_data)
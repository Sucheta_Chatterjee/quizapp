from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class CustomUser(User):
    """
    This model create user . User can be admin or basic user. It will store the profile information
    """
    alt_email = models.EmailField(null=True, blank=True)
    mob_no = models.CharField(max_length=20, null=True, blank=True)
    is_admin = models.BooleanField(default=False)
    
    class Meta:
        verbose_name = 'Custom User'
        db_table = 'quiz_user'
from django.shortcuts import render
from rest_framework.views import APIView
from django.http import HttpResponse
from rest_framework import status
from rest_framework.response import Response
from .models import CustomUser
from django.contrib.auth import authenticate
from django.contrib.auth import login, logout, authenticate
from rest_framework.authentication import SessionAuthentication, TokenAuthentication
from django.http.response import HttpResponseRedirect

# Create your views here.
class SignUp(APIView):
    """
    sign up view set for user creation
    """
    def post(self, request, format=None):
        try:
            first_name = request.data.get("first_name", None)
            last_name = request.data.get("last_name", None)
            mail = request.data.get("mail", None)
            password = request.data.get("password", None)
            alt_email = request.data.get("alt_email", None)
            mob_no = request.data.get("mob_no")
            is_admin = request.data.get("is_admin")
            
            if mail is not None and password is not None:
                if CustomUser.objects.filter(email=mail).exists():
                    return HttpResponse("User is already exists", status=status.HTTP_400_BAD_REQUEST)
                else:
                    user = CustomUser.objects.create_user(
                            first_name=first_name,
                            last_name=last_name,
                            username=first_name,
                            email=mail,
                            alt_email=alt_email,
                            mob_no=mob_no,
                            is_admin=is_admin,
                        )
                    user.is_active = True
                    user.save()
                    token = Token.objects.get_or_create(user=user)
                    
                    return Response({
                        "msg": "user created successfully",
                        "token": token[0].key,
                        }, status=status.HTTP_201_CREATED)
            
        except Exception as e:
            print(e)
            raise ValueError("user data invalid")
        
        
class SignIn(APIView):
    """
    sign up view set for user creation
    """
    def post(self, request, format=None):
        try:
            mail = request.data.get('email', None)
            password = request.data.get('pw', None)
            if password is not None:
                try:
                    user = CustomUser.objects.get(email=mail)
                    if user:
                        authenticate(username=user.username, password=password)
                        if user.is_active:
                            login(request, user)
                            token = Token.objects.get_or_create(user=user)
                        return Response({"token":token[0].key}, status=status.HTTP_200_OK)
                except:
                    raise ValueError("user does not exists")
        except Exception as e:
            print(e)
            return Response({"msg": "user is not valid"}, status=status.HTTP_401_UNAUTHORIZED)
        
class LogOut(APIView):
    """
    log out view
    """
    authentication_classes = (SessionAuthentication, TokenAuthentication)
    
    def get(self, request):
        request.user.auth_token.delete()
        logout(request)
        return HttpResponseRedirect('/sign_in/')
        
        
"""QuizApp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include, url
from django.urls import path, include
from django.views.generic import TemplateView
from custom_user_app.views import SignUp, SignIn, LogOut
from rest_framework import routers
from question_answer_app.views import QuestionCreate

router = routers.SimpleRouter()
router.register(r'question', QuestionCreate, 'question')

urlpatterns = [
    path('admin/', admin.site.urls),
    path('sign_up/', SignUp, name="sign_up"),
    path('log_in/', TemplateView(template_name="sign_in.html"), name="log_in"),
    path('sign_in/', SignIn, name="sign_in"),
    path('logout/', LogOut, name="logout"),
    path('api-auth', include('rest_framework.urls', namespace='rest_framework')),
]
